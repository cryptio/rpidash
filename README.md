# rpidash

Dashcam made using a Raspberry Pi

## Features
- Full FOV resolution of up to 1640 x 922
- Up to 90 FPS (dependent on resolution used)
- Support for loop recording mode (controlled with `segment_video` argument)

## Materials

| Item | Quantity | Price | Optional |
| ------ | ------ | ------ | ------ |
| Raspberry Pi Zero W | 1 | $10 CAD | No |
| 8MP Raspberry Pi Camera V2 | 1 | $35.95 CAD | No |
| Raspberry Pi Zero Mini Camera Cable | 1 | $5 CAD | No |
| UniPiCase Zero - Camera case | 1 | $9.95 CAD | Yes |
| USB A-Male to USB Micro-B cable | 1 | $2.50 CAD (6ft) | No |
| MicroSD card | 1 | $15 CAD (64 GB, Class 10) | No |

**Buy the bundle**:
[BuyaPi Raspberry Pi Zero W Camera Kit](https://www.buyapi.ca/product/raspberry-pi-zero-w-camera-kit-includes-raspberry-pi-zero-w/)

Notes:
- The Raspberry Pi Zero W may be replaced with the non-wireless variant if you are on a tighter budget, however, you will not be able to have your dashcam automatically upload its footage to a server when available to reduce the chances of capacity issues arising.
- Depending on whether the make and model of your car turns off the accessory ports (USB, lighter), you may not be able to capture footage while your vehicle is turned off. If this is the case, then a battery is required in order to do so. The price of the battery is entirely dependent on its capacity and how much power it can deliver at once, so it will not be included in the cost estimate.
- MicroSD cards have become extremely affordable. Make sure you're not underestimating the capacity needed if you're intending on recording at a high frame-rate or high resolution! The price differences between 16 GB, 32 GB, and 64 GB are only a matter of a few dollars.
- While any webcam could technically be used, the Raspberry Pi Camera V2 module is strongly recommended for its cross between cost and quality. The code and instructions present in this repository assume the usage of an official Raspberry Pi camera module. You may also choose to go with the **NoIR** variant, which is the exact same product, except the IR filter has been removed from it. As a result, the dashcam will be more effective in dark lighting, but in the daytime the colors will appear very bland. Luckily, the NoIR camera ships with an optinal IR filter lens that can be applied to combat this.

    IR:

    ![Standard Raspberry Pi camera with IR filter](https://raspi.tv/wp-content/uploads/2013/10/PiNoIR-control-300x168.jpg)

    NoIR:

    ![Raspberry Pi NoIR camera](https://raspi.tv/wp-content/uploads/2013/10/PiNoIR-photo-300x168.jpg)

    *images courtesy of [RasPi.TV](https://raspi.tv/2013/pinoir-whats-it-for-comparison-of-raspicam-and-pi-noir-output-in-daylight)*

## Installation
Run the command `sudo raspi-config` to open the configuration menu for your Raspberry Pi.

![raspi-config screen](https://www.techcoil.com/blog/wp-content/uploads/raspberry-pi-zero-w-raspi-config-raspbian-stretch-lite-with-localisation-options-selected.gif)

The following interfacing options must be enabled:
- Camera
- I2C

In order to use the software, you must have Python3.2 or greater installed as well as pip. pip is used for fetching the Python modules that rpidash depends on. If you are using Raspberry Pi OS (previously called Raspbian), you can install Python3 and pip with the following command:
```bash
sudo apt install python3 python3-pip
```

Next, download the code and navigate to the folder in which it is located. To install Python dependencies, run...
```bash
pip3 install -r requirements.txt
```
Because numpy is used, you will likely need the following additional packages:
```bash
sudo apt-get install python-dev libatlas-base-dev
```

## Usage
### Code
An example of the Python module is as follows:
```python
from rpidash import Dashcam

footage_path = '/home/pi/Videos/dashcam-footage'

with Dashcam(videos_folder=footage_path) as dash:
    try:
        dash.start()

        dash.wait()
    except Exception as e:
        # Handle the exception...
```

It is recommended to use a `with` statement in order to avoid having to manually call `close()` on your `Dashcam` object before you are finished, which frees up resources. This stems from the requirement that, because the class wraps around `picamera`, it has the same constraints.

### Recovering footage
It should be noted that you'll need to convert the raw H.264-encoded video into a container in order for playback to work correctly. 
```bash
$ ffmpeg -r 30 -i "clip-01.h264" "clip-02.mkv"
```

The `-r` parameter, which is 30 in the example command, is the FPS of the recording which ffmpeg uses to create timestamps for the video.

To convert all H264 clips in the current working directory:
```bash
#!/bin/bash

for i in *.h264; do
    [ -f "$i" ] || break

    filename="${i%.*}"
    ffmpeg -r 30 -i $i $filename.mkv
done
```

Observed behavior when trying to play a raw H.264 file via popular software such as VLC media player includes no ability to rewind/fast-forward footage and skipping frames every handful of seconds.

### Running on system startup
If you're going to use your Raspberry Pi as a dashcam, then you'll likely want it to start recording as soon as it's powered on as opposed to having to login to the device each time and run the script. Assuming the Linux distro you're running uses systemd, which most do, you can create a service for rpidash. Create a new file called `rpidash.service` under `/lib/systemd/system` with the following contents:
```ini
[Unit]
Description=rpidash
After=network.target

[Service]
User=pi
Restart=always
WorkingDirectory=/home/pi/rpidash
ExecStart=python3 /home/pi/rpidash/main.py

[Install]
WantedBy=multi-user.target
```

Make sure to replace the path for `WorkingDirectory` and `ExecStart` if your Python project is located elsewhere on the system. Next, you can do `sudo systemctl daemon-reload` followed by `sudo systemctl start rpidash`. The dashcam software should now be running, and will start automatically with the system. Many websites will recommend that you use `rc.local` to run scripts as startup on your Raspberry Pi. It is obsolete, and one issue I personally had was that the script would seemingly exit at random with no indication as to why.

## Advanced
### Input mode selection
The [Picamera docs](https://picamera.readthedocs.io/en/release-1.13/fov.html#sensor-modes) give an excellent explanation of how the resolution and framerate used can determine what input mode the Pi's camera uses, thus changing the FoV (Field of View). Ideally, for a dashcam one would want to have the largest FoV possible. Therefore, the default resoution and framerate is 1640x922 and 30 FPS.

## Disclaimer
rpidash is in no way affiliated with the Raspberry Pi Foundation. rpidash is simply a project that makes use of Raspberry Pi products. Raspberry Pi is a trademark of the Raspberry Pi Foundation. 
