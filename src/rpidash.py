# Copyright 2020 cpke

from picamera import PiCamera, Color, array
from datetime import datetime
import psutil
import logging
import threading
from os import listdir, path
import re
import json

logging.basicConfig(level=logging.INFO)

CAPACITY_LIMIT_PCT = 75


class MotionDetector(array.PiMotionAnalysis):
    def analyze(self, a):
        self.camera.annotate_text = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")


class StatePersistanceManager:
    def __init__(self, filename):
        self.filename = filename
        self.state = {}
        self.load()

    def get(self, key: str):
        return self.state.get(key, None)

    def set(self, key: str, value: any):
        self.state[key] = value

    def remove(self, key: str):
        del self.state[key]

    def load(self):
        try:
            with open(self.filename, "r") as f:
                self.state = json.load(f)
        except FileNotFoundError:
            # No state is currently persisted on the disk, handle gracefully
            pass

    def save(self):
        with open(self.filename, "w+") as f:
            json.dump(self.state, f)


class Dashcam:
    """
    Dashcam is a wrapper class for PiCamera which makes it easy to use your Raspberry Pi as a dashcam
    """

    def __init__(
        self,
        videos_folder: str = "./",
        framerate: int = 30,
        resolution: tuple = (1640, 922),
        segment_video: bool = True,
        no_ir: bool = False,
        segment_len: int = 60,
        segment_max: int = 150,
    ):
        """Create a new Dashcam object.

        Args:
            framerate (int, optional): The framerate to record at. Defaults to 30.
            resolution (tuple, optional): The resolution to record at. Defaults to (1920,1080).
            no_ir (bool, optional): Whether or not the camera being used is NoIR. In this case, the AWB algorithm is set to greyworld (see https://www.raspberrypi.org/forums/viewtopic.php?t=245994). Defaults to False.
            segment_len (int, optional): The number of seconds that each video segment lasts before a new one is started. Defaults to 60.
            segment_max (int, optional): The maximum number of segments to store before rotation happens and the oldest start to be overwritten. Defaults to 150.
        """

        self.videos_folder = videos_folder
        self.filename_prefix = "clip-"

        self.camera = PiCamera()
        if no_ir:
            self.camera.awb_mode = "greyworld"
        self.camera.framerate = framerate
        self.camera.resolution = resolution
        self.camera.rotation = 90

        # We can only change setting the Y' component of a Y'UV tuple (the brightness), so to have a timestamp
        # that is always readable we must set a yellow background instead of foreground
        self.camera.annotate_foreground = Color("white")
        self.camera.annotate_background = Color("black")

        self.segment_video = segment_video
        self.segment_len = segment_len
        self.segment_max = segment_max

        self.recording_segments = False
        self.camera_thread = None
        self.last_clip_no = -1
        self.state_manager = StatePersistanceManager(f"{self.videos_folder}/data.json")

    def __enter__(self):
        return self

    def start_preview(self):
        self.camera.start_preview()

    def stop_preview(self):
        self.camera.stop_preview()

    def start(self):
        """
        Start recording using the dashcam
        """

        self.camera_thread = threading.Thread(target=self._record)
        self.camera_thread.start()

    def _record(self):
        if self.segment_video:
            self.recording_segments = True

            while True:
                self.last_clip_no = -1

                # Make sure that we don't overwrite footage which started a rotation but never
                # reached `segment_max` segments before terminating
                current_clip = self.state_manager.get("current_clip_number")
                if current_clip is not None:
                    self.last_clip_no = int(current_clip)
                else:
                    # If we stopped recording before doing a complete rotation, begin counting from where
                    # we left off
                    for f in listdir(self.videos_folder):
                        f_abs = path.join(self.videos_folder, f)

                        if path.isfile(f_abs) and self.filename_prefix in f:
                            clip_nums = re.findall(
                                f"{self.filename_prefix}(\\d+)[^0-9]*.h264", f
                            )
                            i_val = int("".join(clip_nums))

                            if len(clip_nums) > 0 and i_val > self.last_clip_no:
                                self.last_clip_no = i_val

                # rotate
                if self.last_clip_no + 1 >= self.segment_max:
                    self.last_clip_no = -1


                n = self.last_clip_no + 1

                for filename in self.camera.record_sequence(
                    (
                        self.videos_folder
                        + "/"
                        + self.filename_prefix
                        + "%02d.h264" % i
                        for i in range(self.last_clip_no + 1, self.segment_max)
                    ),
                    motion_output=MotionDetector(self.camera),
                ):

                    if not self.recording_segments:
                        return

                    disk_pct_used = psutil.disk_usage(self.videos_folder).percent

                    if disk_pct_used > CAPACITY_LIMIT_PCT:
                        logging.warning(f"Low disk space ({disk_pct_used}% used)")

                    self.last_clip_no = n

                    self.save_clip_no()

                    logging.info(f"Recording to {filename}")

                    self.camera.wait_recording(self.segment_len)

                    n += 1
        else:
            filename = "{}{}.h264".format(
                self.filename_prefix, datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            )
            logging.info(f"Recording to {filename}")
            # redirect motion vector data so we can annotate each frame
            self.camera.start_recording(
                f"{self.videos_folder}/{filename}",
                motion_output=MotionDetector(self.camera),
            )

    def is_recording(self) -> bool:
        """
        Indicates whether or not the dashcam is currently recording

        Returns:
            bool: True if the dashcam has a recording in progress
        """

        # camera.recording is only True if start_recording() has been called and no call to stop_recording()
        # has been made yet.
        # For segmented recording, we track it by ourselves
        return self.camera.recording or self.recording_segments

    def wait(self):
        self.camera_thread.join()

    def stop(self):
        """
        Stop the recording in progress, blocking until it has finished. If segment_video is True, the method will block until the current segment is finished, for a maximum of segment_len seconds
        """

        if self.segment_video:
            self.recording_segments = False
        else:
            self.camera.stop_recording()

        self.wait()

    def __exit__(self, _exc_type, _exc_value, _traceback):
        self.close()

    def save_clip_no(self):
        self.state_manager.set("current_clip_number", self.last_clip_no)
        self.state_manager.save()

    def close(self):
        self.save_clip_no()

        if self.is_recording():
            self.camera.stop_recording()

        self.camera.close()
